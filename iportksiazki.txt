id, nazwa, autor, isbn, wydawnictwo
1, Dziady cz III, Adam Mickiewicz, 83-86530-14-6
2, Pan Wołodyjowski, Henryk Sienkiewicz, 83-7220-014-9, Zielona Sowa
3, Potop, Henryk Sienkiewicz, 83-7220-009-2, Zielona Sowa
4, Ogniem i Mieczem, Henryk Sienkiewicz, 83-7220-015-7, Zielona Sowa
5. Wojny Cieni, Charles D.Taylor, 83-7082-812-4, Amber
6, Iperium Kości Słoniowej, Naomi Novik, 978-83-7510-176-8, Rebis
7, Śledztwo na cztery ręce Karty na stół, Agata Christie, 83-7129-199-X, Świat książki